#!/usr/bin/python3

import sys

operation = sys.argv[1]
op1 = int(sys.argv[2])
op2 = int(sys.argv[3])

if "suma" == operation:
	res = op1 + op2
	#print(res)
elif "resta" == operation:
	res = op1 - op2
	#print(res)
elif "multiplicacion" == operation:
	res = op1 * op2
	#print(res)
elif "division" == operation:
	try:
		res = op1 / op2
	except ZeroDivisionError:
		print("¡Error! No división entre cero")
		raise SystemExit

print(res)
